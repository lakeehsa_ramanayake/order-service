package com.orderservice.OrderService.services;

import com.orderservice.OrderService.dtos.OrderDTO;
import com.orderservice.OrderService.entities.OrderEntity;
import com.orderservice.OrderService.repositories.OrderRepository;
import com.orderservice.OrderService.utils.Validataions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Order;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Value("${user-service.base-url}")
    private String userServiceBaseURL;

    @Value("${user-service.checkUser-url}")
    private String checkUserURL;



    /*
     * =====================================================================================
     * This method is used to get all Order data
     */
    public List<OrderDTO> getAllDetails(){
        LOGGER.info("================================ Enter into getAllDetails method in OrderService ================================");

        List<OrderDTO> orders = null;
        try {
            orders = orderRepository.findAll()
                    .stream()
                    .map(orderEntity -> new OrderDTO(
                            orderEntity.getId().toString(),
                            orderEntity.getOrderId(),
                            orderEntity.getUserId()
                    )).collect(Collectors.toList());
        }
        catch (Exception e){
            LOGGER.warn("================================ Exception in OrderService -> getAllOrders() - "+e);
        }

        return orders;
    }


    /*
     * =====================================================================================
     * This method is used to get Order data using user ID from the DB
     */
    public List<OrderDTO> getOrdersByUserId(Long id){
        LOGGER.info("================================ Enter into getOrdersByUserId method in OrderService ================================");

        List<OrderDTO> orders = null;
        try{
            orders = orderRepository.findOrdersByUserId(id.toString())
                    .stream()
                    .map(orderEntity -> new OrderDTO(
                            orderEntity.getId().toString(),
                            orderEntity.getOrderId(),
                            orderEntity.getUserId()
                    )).collect(Collectors.toList());
        }
        catch (Exception e){
            LOGGER.warn("================================ Exception in OrderService -> getOrdersByUserId() - "+e);
        }

        return orders;
    }

    /*
     * =====================================================================================
     * This method is used to save a new order to the DB
     */
    public boolean saveOrder(OrderDTO orderDTO){
        LOGGER.info("================================ Enter into saveOrder() in OrderService ================================");
        try{
            if(Validataions.validateNewOrderData(orderDTO)){
                boolean isUserIdExist = Boolean.TRUE.equals(restTemplate.build().getForObject(
                        userServiceBaseURL.concat(checkUserURL).concat("/" + orderDTO.getUserId()),
                        Boolean.class
                ));

                if(isUserIdExist){
                    OrderEntity orderEntity = new OrderEntity(orderDTO.getOrderId(),orderDTO.getUserId());
                    orderRepository.save(orderEntity);
                    return true;
                }
                else {
                    LOGGER.warn("================================ OrderService -> saveOrder() -> No user exist for ID: "+orderDTO.getUserId());
                    return false;
                }
            }
            else {
                return false;
            }
        }
        catch (Exception e) {
            LOGGER.warn("================================ Exception in OrderService -> saveOrder() - "+e.getMessage());
            return false;
        }
    }
}
