package com.orderservice.OrderService.repositories;

import com.orderservice.OrderService.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<OrderEntity,Long> {

    @Query("SELECT o FROM OrderEntity o WHERE o.userId = ?1")
    List<OrderEntity> findOrdersByUserId(String id);
}
