package com.orderservice.OrderService.controllers;


import com.orderservice.OrderService.dtos.OrderDTO;
import com.orderservice.OrderService.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    /*
     * =====================================================================================
     * This method is used to retrieve all users data
     */
    @GetMapping("/getAll")
    public List<OrderDTO> getAllOrders(){

        return orderService.getAllDetails();
    }


    /*
     * =====================================================================================
     * This method is used to retrieve all order data which relevant to a user
     */
    @GetMapping("/getOrdersByUserId/{id}")
    public List<OrderDTO>getOrdersByUserId(@PathVariable final Long id){

        return orderService.getOrdersByUserId(id);
    }

    /*
     * =====================================================================================
     * This method is used to save new order
     */
    @PostMapping("/saveOrder")
    public boolean saveOrder(@RequestBody OrderDTO orderDTO){
        return orderService.saveOrder(orderDTO);
    }

}
